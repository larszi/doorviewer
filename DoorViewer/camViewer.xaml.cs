﻿
using Accord.Video.FFMPEG;
using Notifications.Wpf;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Media;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace DoorViewer
{
    /// <summary>
    /// Interaktionslogik für camViewer.xaml
    /// </summary>
    public partial class camViewer : Window
    {

        VideoFileReader reader = new VideoFileReader();
        NotificationManager notificationManager = new NotificationManager();
        private static List<string> OpenWindowCount = new List<string>();
        private String camShowing;
        static bool runCam = true;
        public static camViewer camViewerGetWindow(String camToShow, bool klingel)
        {
            bool AllowToOpenNewWindow = true;


            switch (OpenWindowCount.Count())
            {
                case 0:

                    break;
                case 1:
                    if (OpenWindowCount.Contains(camToShow))
                    {
                        AllowToOpenNewWindow = false;
                    }
                    break;
                case 2:
                    AllowToOpenNewWindow = false;
                    break;
                default:
                    break;
            }

            if (AllowToOpenNewWindow || klingel)
            {
                return new camViewer(camToShow, klingel);
            }
            else
            {
                return null;
            }
        }
            private camViewer(String camToShow, bool klingel)
            {
         
                OpenWindowCount.Add(camToShow);
                camShowing = camToShow;
                InitializeComponent();
                Thread newWindowThread = new Thread(() => viewStream(camToShow));
                newWindowThread.Start();
                runCam = true;

                if (klingel)
                {
                    bellgif.Opacity = 1;

                    SystemSounds.Hand.Play();

                    notificationManager.Show(new NotificationContent
                    {
                        Title = "Door",
                        Message = "Somebody is at your Door!",
                        Type = NotificationType.Information
                    });

                }
        }
        void button_openDoor_Click(object sender, EventArgs e)
        {
            try
            {
                String MyURI = "http://"+ Properties.Settings.Default.main_ip + "/cgi-bin/accessControl.cgi?action=openDoor&channel=1&UserID=101&Type=Remote";
                WebRequest WReq = WebRequest.Create(MyURI);
                WReq.Credentials = new NetworkCredential("admin", "admin");
                WReq.GetResponse();

                notificationManager.Show(new NotificationContent
                {
                    Title = "Success",
                    Message = "You have Opend Door 1",
                    Type = NotificationType.Success
                });

            }
            catch(Exception)
            {
                notificationManager.Show(new NotificationContent
                {
                    Title = "Error",
                    Message = "Can't Open Door 1",
                    Type = NotificationType.Error
                });
            }
         
        }
        void button_openDoorTwo_Click(object sender, EventArgs e)
        {
            try
            {
                String MyURI = "http://"+ Properties.Settings.Default.main_ip + "/cgi-bin/accessControl.cgi?action=openDoor&channel=2&UserID=101&Type=Remote";
                WebRequest WReq = WebRequest.Create(MyURI);
                WReq.Credentials = new NetworkCredential("admin", "admin");
                WReq.GetResponse();

                notificationManager.Show(new NotificationContent
                {
                    Title = "Success",
                    Message = "You have Opend Door 2",
                    Type = NotificationType.Success
                });

            }
            catch (Exception)
            {
                notificationManager.Show(new NotificationContent
                {
                    Title = "Error",
                    Message = "Can't Open Door 2",
                    Type = NotificationType.Error
                });
            }
        }
        public void viewStream(String camToShow)
        {
            try
            {

            if (camToShow.Equals("IPC"))
            {
                reader.Open(Properties.Settings.Default.ipc_stream);
            }
            else
            {
                String usr = Properties.Settings.Default.username;
                String pwd = Properties.Settings.Default.password;
                String ip = Properties.Settings.Default.main_ip;
                reader.Open("rtsp://"+ usr +":"+ pwd + "@"+ip+"/cam/realmonitor?channel=1&subtype=0");
            }
                Bitmap frame = reader.ReadVideoFrame();
                //Scale to Fit Image
                Bitmap resized = new Bitmap(frame, new System.Drawing.Size(794, 420));

                Application.Current.Dispatcher.BeginInvoke(new Action(() =>
                {
                    loadinggif.Opacity = 0;
                    camFrame.Source = BitmapToImageSource(resized);
                    

                }));


            while (runCam)
            {
                   frame = reader.ReadVideoFrame();
                    //Scale to Fit Image
                   resized = new Bitmap(frame, new System.Drawing.Size(794, 420));

                Application.Current.Dispatcher.BeginInvoke(new Action(() =>
                    {
                        camFrame.Source = BitmapToImageSource(resized);
                    }));
            }
            reader.Close();
            reader.Dispose();
            }
            catch(Exception)
            {
                timeout();
            }
        }
        private void timeout()
        {
            SystemSounds.Exclamation.Play();

            Application.Current.Dispatcher.BeginInvoke(new Action(() =>
            {
                loadinggif.Opacity = 0;
                button_open1.Opacity = 0;
                button_open2.Opacity = 0;
                label_error_cam.Opacity = 1;

                notificationManager.Show(new NotificationContent
                {
                    Title = "Error",
                    Message = "Can't load CAM! Maybe worng IP?",
                    Type = NotificationType.Error
                }); 
            }));
        }
        protected override void OnClosing(CancelEventArgs e)
        {
            OpenWindowCount.Remove(camShowing);
            System.GC.Collect();
            runCam = false;
        }
        BitmapImage BitmapToImageSource(Bitmap bitmap)
        {
            using (MemoryStream memory = new MemoryStream())
            {
                bitmap.Save(memory, System.Drawing.Imaging.ImageFormat.Bmp);
                memory.Position = 0;
                BitmapImage bitmapimage = new BitmapImage();
                bitmapimage.BeginInit();
                bitmapimage.StreamSource = memory;
                bitmapimage.CacheOption = BitmapCacheOption.OnLoad;
                bitmapimage.EndInit();

                return bitmapimage;
            }
        }
    }
}
