# Dahua IP DoorViewer for Windows

A Nativ Windows Software for Dahua IP Doorstations(VTO), it will alert you if somebody rings at your Doorstation as well as show you the Door Camera. You can also add an extra IPC Camera.

## Features

* IP Camera Viewer for the Doorstation and an extra IPC.

* Will show an alert and open a camera stream if somebody rings at your door.

* Door one and Door two can be opened  per button.

* Easy to use user interface, will minimize to the system tray!

* Alert camera stream can be changed to another IPC.

* Multicast and Port can be modified to fit different  Doorstations.

## Download

Standard Windows Installer (64bit)

[DoorViewerSetup.exe](https://libpex.de/data/DoorViewerSetup.exe "DoorViewerSetup.exe")

The exe file can also be found in the Output folder.
## Screenshots

![CamViewer](https://libpex.de/data/cam_ring.png "CamViewer")
### 
Obviously this is not my real camera stream.


![Settings](https://libpex.de/data/settings.png "Settings")

## Dependencies

```
.net Framework 4.6.1
```

## How It Works

If somebody rings at  the Main VTO, the VTO will send Mulicast Packages to all apartment stations via a Multicast (IPV4: 224.0.2.13:30000). The DoorViewer Software will listen for those UDP Packages and show the camera stream.
The Door RTSP Stream URL will be added with credentials to look like this
```
rtsp://username:password@IP/cam/realmonitor?channel=1&subtype=0"
```
The Buttons for opening the Doors are sending GET Requests to this API Endpoint.
The Request will use digest authentication with username and password.
```
"http://IP/cgi-bin/accessControl.cgi?action=openDoor&channel=1&UserID=101&Type=Remote");
```

