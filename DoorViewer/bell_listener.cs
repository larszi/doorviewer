﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;

namespace DoorViewer
{
    class bell_listener
    {
        private static bell_listener bell_Listener = new bell_listener();
        private IPEndPoint endpoint;
        private UdpClient client;
        private IPAddress m_GrpAddr;

        private static bool show = true;
        static bell_listener()
        {
        }

        private bell_listener()
        {
            Thread createBell = new Thread(() => createBellListener());
            createBell.Start();
        }
        public void createBellListener()
        {
            String multi_ip = Properties.Settings.Default.muticast;
            String port = Properties.Settings.Default.multicast_port;

            client = new UdpClient(Int32.Parse(port), AddressFamily.InterNetwork);
            m_GrpAddr = IPAddress.Parse(multi_ip);
            client.JoinMulticastGroup(m_GrpAddr);

            ASCIIEncoding ASCII = new ASCIIEncoding();

            endpoint = new IPEndPoint(IPAddress.Any, 50);

            while (true)
            {
                Byte[] data = client.Receive(ref endpoint);

                if (show)
                {
                    String CamtoShow = Properties.Settings.Default.camShow;
                    //TODO Debug Data Send
                    Console.WriteLine("test");
                    Console.WriteLine(data);
                    Thread waitBellThread = new Thread(() => waitBell());
                    waitBellThread.Start();

                    Application.Current.Dispatcher.BeginInvoke(new Action(() =>
                    {
                        camViewer window_camViewer = camViewer.camViewerGetWindow(CamtoShow, true);
                        window_camViewer.Show();
                    }));
                    show = false;
                }
            }
        }
        private void waitBell() {
            Thread.Sleep(30000);
            System.GC.Collect();
            show = true;
        }

        public static bell_listener Instance
        {
            get
            {
                return bell_Listener;
            }
        }
    }
}
